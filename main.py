from src.operations.sub import SubOperation
from src.operations.sum import SumOperation
from src.calculator import Calculator

calculator = Calculator(SumOperation() , SubOperation())

operation_1 = calculator.addition(2, 5, True)
operation_2 = calculator.subtraction(2,5,True)

print(f'Operacao 1 = {operation_1}')
print(f'Operacao 2 = {operation_2}')